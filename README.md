Block Valley website
====================

Presentation website of **Block Valley** consortium.

Developed by [BTCA Labs](https://btcalabs.tech) a company by [BTC
Assessors](https://btcassessors.com)

# TODO
- Change logo and favicon(s)
- Change colors
